'use strict'

/* **Iteración #2: Buscar la palabra más larga**
 */
/* Completa la función que tomando un array de strings como argumento devuelva el más largo,
en caso de que dos strings tenga la misma longitud deberá devolver el primero.
 */
/* Puedes usar este array para probar tu función:
 */
/* const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];

const avengersList = [];

const longestWord =  (list) =>{
    let result = [];

    for (let i = 0; i < list.length; i++) {
        const name = list[i]; */
        /* console.log(name + ' ' + name.length); */ //comprobar nº letras

/*         if (name.length >= 10) {
            result.push(name);

        }
    }
    return result[0];
}

let res = longestWord(avengers);
console.log (res); */


//-----------------------------//

/* **Iteración #3: Calcular la suma */

/* Calcular una suma puede ser tan simple como iterar sobre un array y sumar cada uno de los elementos.
Implemente la función denominada sumNumbers que toma un array de números como argumento y devuelve
la suma de todos los números de la matriz. */

/* Puedes usar este array para probar tu función:*/

/* const numbers = [1, 2, 3, 5, 45, 37, 58];

function sumAll(paramNumbers) {
    let total = 0;

    for(let i = 0; i < paramNumbers.length; i++) {
        const element = paramNumbers[i];
        total += element;
    }
    return total
}

let sumNumbers = sumAll(numbers);
console.log(sumNumbers); */

//-----------------------//

/* Iteración #4: Calcular el promedio*/
/* Calcular un promedio es una tarea extremadamente común. Puedes usar este array para probar tu función:*/

/* const numbers = [12, 21, 38, 5, 45, 37, 6];

function average(param) {} */


/* **Iteración #6: Valores únicos** */

/* Crea una función que reciba por parámetro un array y compruebe si existen elementos duplicados,
en caso que existan los elimina para retornar un array sin los elementos duplicados.
Puedes usar este array para probar tu función: */


/* const duplicates = [
  'sushi',
  'pizza',
  'burger',
  'potatoe',
  'pasta',
  'ice-cream',
  'pizza',
  'chicken',
  'onion rings',
  'pasta',
  'soda'
];


function removeDuplicates(param) {
    let duplicados = [];
    const tempArray = [...param].sort();

    for (let i = 0; i < tempArray.length; i++) {
      if (tempArray[i + 1] !== tempArray[i]) {
        duplicados.push(tempArray[i]);
      }
    }

    return duplicados;
}

const comiditaDoble = removeDuplicates(duplicates);
console.log(comiditaDoble); */

/* **Iteración #7: Buscador de nombres** */

/* Crea una función que reciba por parámetro un array y el valor que desea comprobar que
existe dentro de dicho array - comprueba si existe el elemento, en caso que existan nos devuelve
un true y la posición de dicho elemento y por la contra un false.
Puedes usar este array para probar tu función: */

/* const nameFinder = [
  'Peter',
  'Steve',
  'Tony',
  'Natasha',
  'Clint',
  'Logan',
  'Xabier',
  'Bruce',
  'Peggy',
  'Jessica',
  'Marc'
];

function finderName(array, elemento) {

console.log(array.includes(elemento));
console.log(array.indexOf(elemento));
}

finderName(nameFinder,'Bruce') */

/* **Iteration #8: Contador de repeticiones** */

/* Crea una función que nos devuelva el número de veces que se repite cada una de
las palabras que lo conforma.Puedes usar este array para probar tu función: */

debugger
const counterWords = [
  'code',
  'repeat',
  'eat',
  'sleep',
  'code',
  'enjoy',
  'sleep',
  'code',
  'enjoy',
  'upgrade',
  'code'
];


function repeatCounter(array) {
  let objList = {};

  array.forEach(element => {
    if (objList[element] === undefined) {
        objList[element] = 1;
// si element no esta creado'undefined', me lo creas  y le asignas valor 1
    }
    else{
      objList[element] += 1;
// si esta creado ya, le sumas 1
    }

});
  return objList
}

console.log(repeatCounter(counterWords));

